#include <cstdio>
#include <cstring>

#define MAX_PLAYERS 510
int scorecard[MAX_PLAYERS + 1];

int main()
{
    int players, rounds, i, j, t, mx, mxpl;

    while(2 == scanf("%d %d", &players, &rounds)) {
        memset(scorecard, 0, MAX_PLAYERS * sizeof(int));

        for (i=0;i<rounds;i++) {
            for (j=0;j<players;j++) {
                scanf("%d", &t);
                scorecard[j] += t;
            }
        }

        mx = 0;
        mxpl = players;
        for (j=players-1;j>=0;j--) {
            if (scorecard[j] > mx) {
                mx = scorecard[j];
                mxpl = j+1;
            }
        }

        printf("%d\n", mxpl);
    }

    return 0;
}
