import java.io.IOException;

class Main {

    public static void main(String args[]) {
        Main myWork = new Main();
        myWork.Begin();
    }

    void Begin() {
        int cases = Integer.parseInt(IO.readLine());
        while (cases > 0) {
            cases --;
            System.out.println(getAnswer(IO.readLine()));
        }
    }

    String getAnswer(String line) {
        int v1 = getValue(line, 26, 0, 2, 'A');
        int v2 = getValue(line, 10, 4, 7, '0');
        return (Math.abs(v1 - v2) <= 100) ? "nice" : "not nice";
    }

    int getValue(String line, int base, int startIndex, int endIndex, char minDigit) {
        int p = endIndex - startIndex;
        int v = 0;
        for (int i = startIndex; i <= endIndex; i++) {
            v += (line.charAt(i) - minDigit) * Math.pow(base, p--);
        }
        return v;
    }
}

class IO {
    static final int MAX_LENGTH_255 = 255;

    static String readLine() {
        return readLine(MAX_LENGTH_255);
    }

    static String readLine(int maxLg) {
        byte lin[] = new byte[maxLg];
        int lg = 0, car = -1;
        String line = "";

        try {
            while (lg < maxLg) {
                car = System.in.read();
                if ((car < 0) || (car == '\n')) break;
                lin[lg++] += car;
            }
        } catch (IOException e) {
            return (null);
        }

        if ((car < 0) && (lg == 0)) return (null);
        return (new String(lin, 0, lg));
    }
}

