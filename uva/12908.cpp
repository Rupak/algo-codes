#include <cstdio>
#include <cstdlib>

#define RANGE 100000000L
// pre calculated
#define N_NUMS 14200

long nums[N_NUMS];

int comp_less_or_equals(const void *a,const void *b)
{
    long *A=(long*)a;
    long *B=(long*)b;

    if(*A>*B && (B-nums == N_NUMS-1 || *A<*(B+1)))
        return 0;

    return *A - *B;
}

int comp_greater_or_equals(const void *a,const void *b)
{
    long *A=(long*)a;
    long *B=(long*)b;

    if(*A<*B && (B-nums == 0 || *A>*(B-1)))
        return 0;

    return *A - *B;
}

int main()
{
    long i, *q, key, tmpkey;

    // generate nums
    for (i=0;i<N_NUMS;i++) {
        nums[i] = (i*(i+1))/2;
    }

    while(1 == scanf("%d", &key) && key) {
        tmpkey = key + 1;
        q=(long*)bsearch(&tmpkey,nums,N_NUMS,sizeof(long),comp_greater_or_equals);
        // if (q) {}
        // checking not required for this problem
        long x = q - nums;
        printf("%ld %ld\n", nums[x]-key, x);
    }

    return 0;
}
