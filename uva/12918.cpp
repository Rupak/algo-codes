#include <cstdio>

int main()
{
    long t, n, m, s, n1, n2, s1, s2;

    scanf("%ld", &t);
    while(t --) {
        scanf("%ld %ld", &n, &m);
        s1 = ((m-1) * m) / 2;
        m = m - n;
        s2 = ((m-1) * m) / 2;
        printf("%ld\n", s1-s2);
    }

    return 0;
}
