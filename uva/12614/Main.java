import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

class Main {

    public static void main(String args[]) {
        Main myWork = new Main();
        myWork.Begin();
    }

    void Begin() {
        int cases = Integer.parseInt(IO.readLine());
        int kase = 1;
        while (cases > 0) {
            cases --;
            int n = Integer.parseInt(IO.readLine());
            String strNumbers = IO.readLine512();
            int[] numbers = ParseUtils.getIntegers(strNumbers);
            Arrays.sort(numbers);
            System.out.println("Case " + kase++ + ": " + numbers[numbers.length - 1]);
        }
    }

    String getAnswer(String line) {
        int v1 = getValue(line, 26, 0, 2, 'A');
        int v2 = getValue(line, 10, 4, 7, '0');
        return (Math.abs(v1 - v2) <= 100) ? "nice" : "not nice";
    }

    int getValue(String line, int base, int startIndex, int endIndex, char minDigit) {
        int p = endIndex - startIndex;
        int v = 0;
        for (int i = startIndex; i <= endIndex; i++) {
            v += (line.charAt(i) - minDigit) * Math.pow(base, p--);
        }
        return v;
    }
}

class ParseUtils {
    public static int[] getIntegers(String line) {
        ArrayList<Integer> listIntegers = new ArrayList<Integer>(0);
        StringTokenizer tokenizer = new StringTokenizer (line);
        while (tokenizer.hasMoreElements()) {
            listIntegers.add(Integer.parseInt(tokenizer.nextToken()));
        }
        int[] integers = new int[listIntegers.size()];
        for (int i=0;i<integers.length;i++) {
            integers[i] = listIntegers.get(i).intValue();
        }
        return integers;
    }
}
class IO {
    static final int MAX_LENGTH_256 = 256;
    static final int MAX_LENGTH_512 = 512;

    static String readLine() {
        return readLine(MAX_LENGTH_256);
    }

    static String readLine512() {
        return readLine(MAX_LENGTH_512);
    }

    static String readLine(int maxLg) {
        byte lin[] = new byte[maxLg];
        int lg = 0, car = -1;
        String line = "";

        try {
            while (lg < maxLg) {
                car = System.in.read();
                if ((car < 0) || (car == '\n')) break;
                lin[lg++] += car;
            }
        } catch (IOException e) {
            return (null);
        }

        if ((car < 0) && (lg == 0)) return (null);
        return (new String(lin, 0, lg));
    }
}

