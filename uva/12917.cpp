#include <cstdio>

int main()
{
    int p, h, o;

    while(3 == scanf("%d %d %d", &p, &h, &o)) {
        if ( (h - (o-p)) > 0 ) {
            printf("Hunters win!\n");
        } else {
            printf("Props win!\n");
        }
    }

    return 0;
}
