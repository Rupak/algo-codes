#include <cstdio>

int main()
{
    int f[10], i, j, r, N, ans;

    for (int i=0;i<10;i++) {
        f[i] = 1;
        for (int j=i;j>=2;j--) {
            f[i] *= j;
        }
    }

    while(1 == scanf("%d", &N)) {
        ans = 0;
        for (i=9;i>=0;i--) {
            r = N / f[i];
            if (r > 0) {
                N = N - f[i]*r;
                ans += r;
            }
        }
        printf("%d\n", ans);
    }

    return 0;
}
