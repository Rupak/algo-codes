#include <cstdio>
#include <cstring>
#include <cstdlib>

int main()
{
    int n, d, len, i, j;
    char line[100], words[3][10] = {"one", "two", "three"};

    gets(line);
    n = atoi(line);
    while (n--) {
        gets(line);
        len = strlen(line);

        for (i=0;i<3;i++) {
            if (strlen(words[i]) == len) {
                d = 0;
                for (j=0;j<len;j++) {
                    if (words[i][j] != line[j]) {
                        d ++;
                    }
                }
                if (d <= 1) {
                    printf("%d\n", i+1);
                    i = 4;
                    break;
                }
            }
        }
    }
    return 0;
}
