import java.math.BigInteger;


public class Main {
	public static void main(String args[]) {
		 BigInteger num = new BigInteger ("1");
		 BigInteger result = new BigInteger ("1");
		 BigInteger one = new BigInteger ("1");
		 int i, sum = 0;
		 for (i=1;i<=100;i++) {
			 result = result.multiply(num);
			 num = num.add(one);
		 }

		 String str = result.toString();
		 for (i=0;i<str.length();i++) {
			 sum += str.charAt(i) - '0';
		 }
		 System.out.println(sum);
	}
}
